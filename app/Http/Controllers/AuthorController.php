<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function showAllAuthors()
    {
        // The showAllAuthors method checks for all the author resources.

        return response()->json(Author::all());
    }

    public function showOneAuthor($id)
    {
        // The showOneAuthor method checks for a single author resource.

        return response()->json(Author::find($id));
    }

    public function create(Request $request)
    {
        // The create method creates a new author resource.
        // Validate incoming data
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'location' => 'required|alpha'
        ]);

        $author = Author::create($request->all());

        return response()->json($author, 201);
    }

    public function update($id, Request $request)
    {
        // The update method checks if an author resource exists and allows the resource to be updated.

        $author = Author::findOrFail($id);
        $author->update($request->all());

        return response()->json($author, 200);
    }

    public function delete($id)
    {
        // The delete method checks if an author resource exists and deletes it.

        Author::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    // response() is a global helper function that obtains an instance of the response factory. 
    // response()->json() simply returns the response in JSON format.
    /**
     * 200 is an HTTP status code that indicates the request was successful.
     * 201 is an HTTP status code that indicates a new resource has just been created.
     * findOrFail method throws a ModelNotFoundException if no result is not found.
     */
}
