# Lumen API With Auth0

- This is basic implementation about how to integrate authentication using Auth0

- Sample pictures are below :

![alt text](/screenshots/post.png "POST")
![alt text](/screenshots/get.png "GET")
![alt text](/screenshots/put.png "PUT")
![alt text](/screenshots/delete.png "DELETE")